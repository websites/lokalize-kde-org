<?php
 $page_title = "Lokalize computer-aided translation system";
 $teaser = false;
 include "header.inc";
?>

<img src="https://userbase.kde.org/images.userbase/a/ab/Lokalize.png" class="noborder" style="width: 128px; height: 128px; float: right; margin-bottom: 0px; border: none;" alt="Lokalize" />

<p>
Lokalize is the localization tool made by <a href="http://www.kde.org">KDE</a>.
</p>

<p>
Lokalize is also a general computer-aided translation system (CAT) with which you can translate OpenDocument files (*.odt).
Translate-Toolkit is used internally to extract text for translation from .odt to .xliff files and to merge translation back into .odt file.
</p>

<p>
You can find some more information at <a href="https://www.kde.org/applications/development/lokalize/">https://www.kde.org/applications/development/lokalize/</a>
</p>

<?php
 include "footer.inc";
?>
